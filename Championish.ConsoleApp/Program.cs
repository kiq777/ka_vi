﻿using System;
using System.Collections.Generic;
using System.Linq;
using Championish.Infra.Data;
using Championish.Infra.Data.Models;
using Championish.Services.Services;
using Championish.Utils;
using Microsoft.EntityFrameworkCore;

namespace Championish.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new ChampionishDbContext())
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                TeamService.Seed();

                var teams = TeamService.GetRandomizedTeams();                
                GroupService.SortTeamGroups(teams);
                ShowTeams(teams);

                var groups = GroupService.GetGroups();
                ShowGroupsTeams(groups);
                MatchService.GenerateGroupStageMatches(groups);
                
                var groupMatches = MatchService.GetGroupMatches();
                MatchService.PlayGroupMatches(groupMatches);
                ShowGroupMatches(groupMatches);

                teams = TeamService.GetTeamsOrderedByRoundsAndScore();
                ShowTeamsStats(teams);
                
                var playoffsTeams = PlayoffService.GetTeamsPlayoffs();
                PlayoffService.GeneratePlayoffsMatches(playoffsTeams);
                ShowPlayoffsTeams(playoffsTeams);

                var playoffsMatches = PlayoffService.GetPlayoffMatches();
                ShowPlayoffStats(playoffsMatches);
        
                var winner = PlayoffService.GetWinner();
                ShowWinner(winner);
            }
        }

        #region Show methods

        static void ShowTeams(List<Team> teams)
        {
            Console.WriteLine("===========");
            Console.WriteLine("TIMES DO CAMPEONATO");
            teams.ForEach(x => Console.WriteLine($"{x.Name}"));
        } 

        static void ShowGroupsTeams(List<Group> groups)
        {
            Console.WriteLine("===========");
            Console.WriteLine("SORTEIO DOS GRUPOS");
            
            groups.ForEach(group =>
            {
                Console.WriteLine("===========");
                Console.WriteLine($"Grupo: {group.Name}");

                foreach(var team in group.Teams)
                    Console.WriteLine($"{team.Team.Name}");
            });
        }

        static void ShowGroupMatches(List<Match> matches)
        {
            Console.WriteLine("===========");
            Console.WriteLine("RESULTADO DAS PARTIDAS (GRUPO)");
           
            matches.ForEach(match =>
            {
                Console.WriteLine("===========");
                Console.WriteLine($"{match.TeamA.Name} - {match.TeamARounds} vs {match.TeamBRounds} - {match.TeamB.Name}");
            });
        }

        static void ShowTeamsStats(List<Team> teams)
        {
            Console.WriteLine("===========");
            Console.WriteLine("STATUS DOS TIMES (VITORIAS/SALDO DE ROUNDS)");
            
            teams.ForEach(team =>
            {
                Console.WriteLine("===========");
                Console.WriteLine($"Nome: {team.Name} - Saldo de rounds: {team.Rounds} - Vitorias: {team.Score}");
            });
        }

        static void ShowPlayoffsTeams(List<Team> teams)
        {
            Console.WriteLine("===========");
            Console.WriteLine("TIMES QUE PASSARAM PARA AS PLAYOFFS");
            teams.ForEach(team => Console.WriteLine($"{team.Name}"));
        }

        static void ShowPlayoffStats(List<Playoff> playoffs)
        {
            Console.WriteLine("===========");
            Console.WriteLine("RESULTADO PLAYOFFS");
            
            playoffs.ForEach(match =>
            {
                Console.WriteLine("===========");
                Console.WriteLine($"Fase: {match.Phase}: {match.TeamA.Name} - {match.TeamARounds} vs {match.TeamBRounds} - {match.TeamB.Name}");
            });
        }

        static void ShowWinner(Team team)
        {
            Console.WriteLine("===========");
            Console.WriteLine($"Time campeão: {team.Name}");
        }

        #endregion
    }
}