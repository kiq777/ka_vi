using System;

namespace Championish.Infra.Data.Models
{
    public class Match
    {
        public int Id { get; set; }
        public int TeamAId { get; set; }
        public int TeamBId { get; set; }
        public int TeamARounds { get; set; }
        public int TeamBRounds { get; set; }
        public virtual Team TeamA { get; set; }
        public virtual Team TeamB { get; set; }
    }
}