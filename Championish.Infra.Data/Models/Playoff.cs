using Championish.Common.Enums;

namespace Championish.Infra.Data.Models
{
    public class Playoff
    {
        public int Id { get; set; }
        public int TeamAId { get; set; }
        public int TeamARounds { get; set; }
        public int TeamBId { get; set; }
        public int TeamBRounds { get; set; }
        public EPhase Phase { get; set; }
        public Team TeamA { get; set; }
        public Team TeamB { get; set; }
    }
}