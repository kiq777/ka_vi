using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Championish.Infra.Data.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
        public int Rounds { get; set; }
        public GroupTeam Group { get; set; }
    }
}