using System;
using System.Collections;
using System.Collections.Generic;

namespace Championish.Infra.Data.Models
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<GroupTeam> Teams { get; set; } = new List<GroupTeam>();
    }
}