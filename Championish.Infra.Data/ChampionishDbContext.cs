using System;
using Championish.Infra.Data.Mappings;
using Championish.Infra.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Microsoft.Extensions.Configuration.Json;

namespace Championish.Infra.Data
{
    public class ChampionishDbContext : DbContext
    {
        private readonly string _sqlConnection;
        
        public ChampionishDbContext()
        {
            _sqlConnection = Environment.GetEnvironmentVariable("DB_CONNECTION_STRING");
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_sqlConnection);
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new GroupMap());
            modelBuilder.ApplyConfiguration(new MatchMap());
            modelBuilder.ApplyConfiguration(new PlayoffMap());
            modelBuilder.ApplyConfiguration(new TeamMap());
            
            modelBuilder.ForNpgsqlUseIdentityColumns();
        }

        public DbSet<Match> Matches { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Playoff> Playoffs { get; set; }
    }
}    