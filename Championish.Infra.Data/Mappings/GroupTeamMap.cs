using Championish.Infra.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Championish.Infra.Data.Mappings
{
    public class GroupTeamMap : IEntityTypeConfiguration<GroupTeam>
    {
        public void Configure(EntityTypeBuilder<GroupTeam> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.GroupId).IsRequired();
            builder.Property(x => x.Team).IsRequired();

            builder
                .HasOne(x => x.Group)
                .WithMany(x => x.Teams)
                .HasForeignKey(x => x.GroupId)
                .HasForeignKey(x => x.TeamId);
        }
    }
}