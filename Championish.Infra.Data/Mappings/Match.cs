using Championish.Infra.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Championish.Infra.Data.Mappings
{
    public class MatchMap : IEntityTypeConfiguration<Match>
    {
        public void Configure(EntityTypeBuilder<Match> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.TeamAId).IsRequired();
            builder.Property(x => x.TeamARounds).IsRequired();
            builder.Property(x => x.TeamBId).IsRequired();
            builder.Property(x => x.TeamBRounds).IsRequired();
        }
    }
}