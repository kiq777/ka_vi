using Championish.Infra.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Championish.Infra.Data.Mappings
{
    public class PlayoffMap : IEntityTypeConfiguration<Playoff>
    {
        public void Configure(EntityTypeBuilder<Playoff> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.TeamAId).IsRequired();
            builder.Property(x => x.TeamARounds).IsRequired();
            builder.Property(x => x.TeamBId).IsRequired();
            builder.Property(x => x.TeamBRounds).IsRequired();
        }
    }
}