# Championish
[![Build status](https://ci.appveyor.com/api/projects/status/lbrrqv087efpad3u?svg=true)](https://ci.appveyor.com/project/kiq7/ka-vi)

An simple app to simulate a championish.

## Getting Started

### Prerequisites

* .NET Core 2.1
* Postgres
* Docker

## Running project

### Checkout this repository 

```
git clone https://kiq7@bitbucket.org/kiq7/ka_vi.git
```

after that, run the docker-compose command to run application and the database:

```
docker-compose up
```

The application runs at console. If your terminal is unable to list all lines, you can export the result as .txt at project directory.

```
docker-compose up > results.txt
```

## Built With

* [.NET Core](https://www.microsoft.com/net/learn/get-started)
* Entity Framework Core
* Postgres

## Author

* **Kaique Vieira**  

## License

This project is licensed under the MIT License