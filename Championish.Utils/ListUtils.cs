﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Championish.Utils
{
    public static class ListUtils
    {
        public static List<T> Randomize<T>(List<T> list)
        {
            var rnd = new Random();
            return list
                .Select(x => new {value = x, order = rnd.Next()})
                .OrderBy(x => x.order).Select(x => x.value).ToList();
        }
        
        public static List<List<T>> ChunkBySize<T>(List<T> items, int chunkSize)
        {
            return items
                .Select((x, i) => new {Index = i, Value = x})
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
    }
}