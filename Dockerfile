FROM microsoft/dotnet:2.1.500-sdk as dotnet-restore
WORKDIR /project
COPY . .
RUN dotnet restore

FROM dotnet-restore AS publish
RUN dotnet publish ./Championish.ConsoleApp --no-restore -c Release -o /published

FROM microsoft/dotnet:2.1-aspnetcore-runtime AS release
WORKDIR /app
COPY --from=publish /published .
ENTRYPOINT ["dotnet", "Championish.ConsoleApp.dll"]
