namespace Championish.Common.Enums
{
    public enum EPhase : int
    {
        SixteenthFinals = 1,
        EighthFinals = 2,
        QuarterFinals = 3,
        SemiFinals = 4,
        Final = 5
    }
}