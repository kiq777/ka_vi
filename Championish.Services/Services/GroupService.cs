using System.Collections.Generic;
using System.Linq;
using Championish.Infra.Data;
using Championish.Infra.Data.Models;
using Championish.Utils;
using Microsoft.EntityFrameworkCore;

namespace Championish.Services.Services
{
    public class GroupService
    {
        public static List<Group> SortTeamGroups(List<Team> teamsList)
        {
            var listTeamsChunked = ListUtils.ChunkBySize(teamsList, 5);

            var groups = new List<Group>();

            foreach (var teams in listTeamsChunked)
            {
                var group = new Group
                {
                    Name = $"Group {listTeamsChunked.IndexOf(teams) + 1}"
                };

                foreach (var team in teams)
                {
                    group.Teams.Add(new GroupTeam
                    {
                        GroupId = group.Id,
                        TeamId = team.Id
                    });
                }

                groups.Add(group);
            }

            using (var context = new ChampionishDbContext())
            {
                context.Groups.AddRange(groups);
                context.SaveChanges();
            }

            return groups;
        }

        public static List<Group> GetGroups()
        {
            using (var context = new ChampionishDbContext())
            {
                return context
                    .Groups
                    .Include(x => x.Teams)
                    .ThenInclude(x => x.Team).ToList();
            }
        }
    }
}