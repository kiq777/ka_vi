using System.Collections.Generic;
using System.Linq;
using Championish.Common.Enums;
using Championish.Infra.Data;
using Championish.Infra.Data.Models;
using Championish.Utils;
using Microsoft.EntityFrameworkCore;

namespace Championish.Services.Services
{
    public class PlayoffService
    {
        private static EPhase GetPhaseByTeamsQuantity(int teamsQuantity)
        {
            switch (teamsQuantity)
            {
                case 32:
                    return EPhase.SixteenthFinals;

                case 16:
                    return EPhase.EighthFinals;

                case 8:
                    return EPhase.QuarterFinals;

                case 4:
                    return EPhase.SemiFinals;

                case 2:
                    return EPhase.Final;

                default:
                    return 0;
            }
        }
        
        public static List<Team> GetTeamsPlayoffs()
        {
            using (var context = new ChampionishDbContext())
            {
                var teams = context.Teams
                    .OrderByDescending(x => x.Rounds)
                    .ThenByDescending(x => x.Score)
                    .Take(32)
                    .ToList();

                return ListUtils.Randomize(teams);                
            }
        }
        
        public static void GeneratePlayoffsMatches(List<Team> teams)
        {
            var playoffs = new List<Playoff>();
            var teamsCount = teams.Count;

            var phase = GetPhaseByTeamsQuantity(teamsCount);

            foreach (var team in teams)
            {
                if (playoffs.Any(x => x.TeamBId == team.Id))
                    continue;

                var playoff = new Playoff
                {
                    TeamAId = team.Id,
                    TeamBId = teams[teams.IndexOf(team) + 1].Id,
                    Phase = phase
                };

                playoffs.Add(playoff);
            }

            using (var context = new ChampionishDbContext())
            {
                context.Playoffs.AddRange(playoffs);
                context.SaveChanges();
                
                var matches = context.Playoffs.Include(x => x.TeamA).Include(x => x.TeamB).Where(x => x.Phase == phase)
                    .ToList();

                PlayPlayoffMatches(matches);
                var winners = GetPlayoffsWinners(playoffs);

                if(teamsCount > 2)
                    GeneratePlayoffsMatches(winners);
            }
        }
        
        private static void PlayPlayoffMatches(List<Playoff> playoffs)
        {
            foreach (var match in playoffs)
            {
                var matchResult = MatchService.GetMatchResult();

                match.TeamARounds = matchResult.First();
                match.TeamBRounds = matchResult.Last();

                var winner = match.TeamARounds > match.TeamBRounds ? match.TeamA : match.TeamB;

                winner.Score += 1;

                using (var context = new ChampionishDbContext())
                {
                    context.Set<Playoff>().Update(match);
                    context.SaveChanges();
                }
            }
        }
        
        private static List<Team> GetPlayoffsWinners(List<Playoff> matches)
        {
            var teams = new List<Team>();

            foreach (var match in matches)
            {
                var winner = match.TeamARounds > match.TeamBRounds ? match.TeamA : match.TeamB;
                teams.Add(winner);
            }

            return teams;
        }

        public static List<Playoff> GetPlayoffMatches()
        {
            using (var context = new ChampionishDbContext())
                return context.Playoffs.Include(x => x.TeamA).Include(x => x.TeamB).OrderBy(x => x.Id).ToList();
        }

        public static Team GetWinner()
        {
            using (var context = new ChampionishDbContext())
            {
                var finals = context.Playoffs.Include(x => x.TeamA).Include(x => x.TeamB).FirstOrDefault(x => x.Phase == EPhase.Final);

                return finals.TeamARounds > finals.TeamBRounds ? finals.TeamA : finals.TeamB;
            }
        }
    }
}