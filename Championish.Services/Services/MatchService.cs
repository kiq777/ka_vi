using System;
using System.Collections.Generic;
using System.Linq;
using Championish.Infra.Data;
using Championish.Infra.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Championish.Services.Services
{
    public class MatchService
    {
        public static List<int> GetMatchResult()
        {
            var random = new Random();

            var result = new List<int>
            {
                random.Next(0, 16),
                random.Next(0, 16)
            };

            var indexMaxResult = result.IndexOf(result.Max());
            result[indexMaxResult] = 16;

            if (result.TrueForAll(x => x == 16))
                GetMatchResult();

            return result;
        }
        
        public static void GenerateGroupStageMatches(List<Group> groups)
        {
            var matches = new List<Match>();

            foreach (var group in groups)
            {
                foreach (var team in group.Teams)
                {
                    var teamMatches = group.Teams
                        .Where(x => x.Id != team.Id)
                        .Select(x =>
                            new Match
                            {
                                TeamAId = team.TeamId,
                                TeamBId = x.TeamId,
                            }).ToList();

                    matches.AddRange(teamMatches);
                }
            }

            using (var context = new ChampionishDbContext())
            {
                context.Matches.AddRange(matches);
                context.SaveChanges();
            }
        }

        public static List<Match> GetGroupMatches()
        {
            using (var context = new ChampionishDbContext())
            {
                return context.Matches
                    .Include(x => x.TeamA)
                    .Include(x => x.TeamB)
                    .ToList();
            }
        }
        
        public static void PlayGroupMatches(List<Match> matches)
        {
            foreach (var match in matches)
            {
                var matchResult = MatchService.GetMatchResult();

                match.TeamARounds = matchResult.First();
                match.TeamBRounds = matchResult.Last();

                match.TeamA.Rounds += match.TeamARounds;
                match.TeamB.Rounds += match.TeamBRounds;

                var winner = match.TeamARounds > match.TeamBRounds ? match.TeamA : match.TeamB;

                winner.Score += 1;

                using (var context = new ChampionishDbContext())
                {
                    context.Set<Match>().Update(match);
                    context.SaveChanges();
                }
            }
        }
    }
}