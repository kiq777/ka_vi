using System.Collections.Generic;
using System.Linq;
using Championish.Infra.Data;
using Championish.Infra.Data.Models;
using Championish.Utils;

namespace Championish.Services.Services
{
    public class TeamService
    {
        public static void Seed()
        {
            var teamNames = new[]
            {
                "100 Thieves",
                "Astralis",
                "AVANGAR",
                "BIG",
                "Bravado Gaming",
                "Cloud9",
                "compLexity Gaming",
                "Denial eSports",
                "DETONA Gaming",
                "Devils.one",
                "Echo Fox",
                "ECL.gg",
                "ENCE eSports",
                "Envyus",
                "eUnited",
                "Evidence e-Sports",
                "FaZe Clan",
                "fnatic",
                "forZe",
                "G2 Esports",
                "G3X",
                "Gambit Esports",
                "Games Academy",
                "Ghost Gaming",
                "HellRaisers",
                "Heroic",
                "Immortals",
                "Imperial e-Sports",
                "INTZ eSports",
                "INTZ.Academy",
                "Isurus Gaming",
                "Kabum",
                "Keyd",
                "Luminosity Gaming",
                "MIBR",
                "mousesports",
                "MVP PK",
                "Myth",
                "Natus Vincere",
                "Ninjas in Pyjamas",
                "NoChance",
                "North",
                "NRG eSports",
                "Old Guys Club",
                "OpTic Gaming",
                "ORDER",
                "paiN Gaming",
                "playArt",
                "Red Reserve",
                "Renegades",
                "Rogue",
                "Sharks Esports",
                "SK Gaming",
                "Space Soldiers",
                "Swole Patrol",
                "Team Dignitas",
                "Team LDLC",
                "Team Liquid",
                "Team One",
                "Team Spirit",
                "Team Vitality",
                "Team Wild",
                "Tempo Storm",
                "Titan",
                "TYLOO",
                "Uruguay3",
                "Valiance & Co",
                "VAULT",
                "Vega Squadron",
                "Vici Gaming",
                "Virtue Gaming",
                "Virtus.pro",
                "W7M Gaming",
                "WePlayGames",
                "Windigo Gaming",
                "WinOut Gaming",
                "Winstrike Team",
                "Winterfox",
                "x6tence",
                "YeaH Gaming"
            };

            var teams = teamNames.Select(team => new Team
            {
                Name = team
            }).ToList();

            using (var context = new ChampionishDbContext())
            {
                context.Teams.AddRangeAsync(teams);
                context.SaveChanges();
            }
        }

        public static List<Team> GetRandomizedTeams()
        {
            using (var context = new ChampionishDbContext())
            {
                var teams = context.Teams.ToList();
                return ListUtils.Randomize(teams);
            }
        }

        public static List<Team> GetTeamsOrderedByRoundsAndScore()
        {
            using (var context = new ChampionishDbContext())
            {
                return context.Teams
                    .OrderByDescending(x => x.Score)
                    .ThenByDescending(x => x.Rounds)
                    .ToList();
            }
        }
        
        public static List<Team> GetTeamsOrderedById()
        {
            using (var context = new ChampionishDbContext())
            {
                return context.Teams
                    .OrderBy(x => x.Id)
                    .ToList();
            }
        }
    }
}